import {Container} from 'react-bootstrap';
import {useState} from 'react'
import  {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import AppNavbar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/Error'
import { UserProvider } from './UserContext';
import './App.css';

function App() {

// React Context is nothing but global state to the app. It is a way to make particular data available to all the components no matter how they are nested
  // State hook for teh user state taht
  // Initialiuzed as an object with properties from the  local storage
  // This will be used to store the user info and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  //Function for clearing the localStorage on logout


  const unsetUser = () => {
    localStorage.clear()
  }
  
  useEffect(()=> {

      fetch("http://localhost:4000/api/users/details", {
        headers: {
          Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {

        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })
  
  }, [])



  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element ={<Home />} />
            <Route path="/courses" element ={<Courses />} />
            <Route path="/login" element ={<Login />} />
            <Route path="/register" element ={<Register />} />
            <Route path="/logout" element ={<Logout />} />
            <Route path='*' element={<ErrorPage/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
