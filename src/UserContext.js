import React from 'react'

// Create a Contezt Object
// a context object as the name states is a data type of an iibject that can be used to store information that can be shared to other components within the app
// THe context object is a different approach to passing information between components and allows easier access by avoidin the use of prop-driling
const UserContext = React.createContext()

export const UserProvider = UserContext.Provider;


export default UserContext;