import {useState} from 'react';
import { Card, Button } from "react-bootstrap";

export default function CourseCard({courseProp}) {


	// const [count, setCount] = useState(0)
	// function enroll() {
	// 	setCount(count + 1)
	// 	// if (count === 30) {
	// 	// 	alert("NO MORE SEATS!")
	// 	// }
	// }

	// const [seat, setSeat] = useState(30)
	// function seats() {
	// 	setSeat(seat - 1)
	// 	if (seat === 0) {
	// 		alert("NO MORE SEATS!")
	// 	}		
	// }



	const {name, description, price, _id} = courseProp


	return(

			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Button variant="primary" as={Link} to={`/courses/${_id}`} >Details</Button>
				</Card.Body>
			</Card>
		)

}
