const coursesData = [
	{
		id: "wdc01",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc02",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc03",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;