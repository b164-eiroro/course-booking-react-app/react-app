import { useState, useEffect, useContext} from 'react';
import { Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom'
export default function Register() {

	const {user, setUser} = useContext(UserContext)
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	//State to determine whether the button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	function registerUser(e) {

		//prevents page redirection via a form submission
		e.preventDefault();

		fetch("http://localhost:4000/api/users/checkEmail", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data) // true/false

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration"
				})

			} else {

				fetch("http://localhost:4000/api/users/register", {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {

						//Clear input fields
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Welcome to Batch 164, Course Booking!'
						})

						navigate("/login")

					} else {

						Swal.fire({
							title: 'Something went wrong!',
							icon: 'error',
							text: 'Please try again.'
						})

					}


				})


			}

		})


	}





	useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 &&  email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])

	return(


		(user.id !== null) ?


		<Navigate to="/courses" />

		: 


		<Form onSubmit={(e) => registerUser(e)}>
		<h1 className="text-center">Register</h1>
		  <Form.Group 
		  		className="mb-3" 
		  		controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	value={email}
		    	onChange={e => {
		    		setEmail(e.target.value)
		    		//console.log(e.target.value)
		    	}} 
		    	required
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value={password1}
		    	onChange={e => setPassword1(e.target.value)}
		    	required
		    />
		  </Form.Group>
		  
		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password2}
		    	onChange={e => setPassword2(e.target.value)}
		    	required 
		    />
		  </Form.Group>

		{/*Conditionallu render submit button based on the isActive state*/}
		  {
		  	isActive ?
		  	<Button variant="primary" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>

		  }



		</Form>


		)


}
